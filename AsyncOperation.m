//
//  AsyncOperation.m
//  Downloader
//
//  Created by LAP12230 on 3/2/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "AsyncOperation.h"

@interface AsyncOperation () {
    BOOL _finished;
    BOOL _executing;
}

@property (nonatomic, copy) AsyncExecutionBlock executionBlock;

@end

@implementation AsyncOperation

#pragma mark - Life Cycles

- (instancetype)initWithAsyncBlock:(AsyncExecutionBlock)executionBlock {
    
    if (!executionBlock) {
        return nil;
    }
    
    if ((self = [super init])) {
        _executionBlock = executionBlock;
        _finished = false;
        _executing = false;
    }
    
    return self;
}


- (void)start {
    
    if (self.isCancelled) {
        [self setFinished:true];
        return;
    }
    
    [self executeTask];
    [self setExcuting:true];
}


- (void)finished {
    [self setFinished:true];
    [self setExcuting:false];
}


- (void)main {
    [super main];
}


+ (instancetype)initWithAsyncBlock:(AsyncExecutionBlock)executionBlock {
    return [[AsyncOperation alloc] initWithAsyncBlock:executionBlock];
}

#pragma mark - Custom setters, getters

- (void)setFinished:(BOOL)value {
    [self willChangeValueForKey:@"isFinished"];
    _finished = value;
    [self didChangeValueForKey:@"isFinished"];
}


- (void)setExcuting:(BOOL)value {
    [self willChangeValueForKey:@"isExcuting"];
    _executing = value;
    [self didChangeValueForKey:@"isExcuting"];
}


- (BOOL)isAsynchronous {
    return true;
}


- (BOOL)isConcurrent {
    return true;
}


- (BOOL)isExecuting {
    return _executing;
}


- (BOOL)isFinished {
    return _finished;
}

#pragma mark - Private Methods

- (void)executeTask {
    
    __weak typeof (self) weakSelf = self;
    self.executionBlock(^{
        [weakSelf finished];
    });
}

@end
