//
//  AsyncOperation.h
//  Downloader
//
//  Created by LAP12230 on 3/2/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^SignalFinishBlock)(void);
typedef void (^AsyncExecutionBlock)(SignalFinishBlock);

@interface AsyncOperation : NSOperation

- (instancetype)initWithAsyncBlock:(AsyncExecutionBlock)executionBlock;
+ (instancetype) initWithAsyncBlock:(AsyncExecutionBlock)executionBlock;

@end

NS_ASSUME_NONNULL_END
