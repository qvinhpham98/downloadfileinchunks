//
//  DownloadWorker.h
//  Downloader
//
//  Created by LAP12230 on 2/27/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^CompletionHandler)(NSURL *);
typedef void (^UpdateProgressHandler)(unsigned long long, unsigned long long, unsigned long long);


@interface DownloadWorker : NSObject <NSURLSessionDownloadDelegate>

@property (nonatomic, readonly) NSURL *url;
@property (nonatomic, readonly) NSURL *destination;
@property (nonatomic, readonly) NSURLRequest *request;
@property (nonatomic, readonly) NSString *fileName;

- (void)startDownloadTo:(NSURL * _Nullable)destination
               fileName:(NSString * _Nullable)fileName
      completionHandler:(CompletionHandler)completionHandler
  updateProgressHandler:(UpdateProgressHandler)progressHandler;

- (void)startDownloadTo:(NSURL *)destination
    completionHandler:(CompletionHandler)completionHandler
updateProgressHandler:(UpdateProgressHandler)progressHandler;

- (void)startDownloadWithCompletionHandler:(CompletionHandler)completionHandler
updateProgressHandler:(UpdateProgressHandler)progressHandler;

+ (instancetype)downloadWithURL:(NSURL *)url;
+ (instancetype)downloadWithURL:(NSURL *)url destination:(NSURL *)destination;
+ (instancetype)downloadWithURL:(NSURL *)url destination:(NSURL *)destination fileName:(NSString *)fileName;

+ (instancetype)downloadWithRequest:(NSURLRequest *)request;
+ (instancetype)downloadWithRequest:(NSURLRequest *)request destination:(NSURL *)destination;
+ (instancetype)downloadWithRequest:(NSURLRequest *)request destination:(NSURL *)destination fileName:(NSString *)fileName;

@end

NS_ASSUME_NONNULL_END
