//
//  DownloadManagerCenter.h
//  Downloader
//
//  Created by LAP12230 on 2/28/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadTaskInfo.h"

@class DownloadManager;

NS_ASSUME_NONNULL_BEGIN

@protocol DownloadManagerDelegate <NSObject>

- (void)downloadManager:(DownloadManager *)downloadManager didFinishDownloadAndSaveAtURL:(NSURL *)url forDownloadTaskInfoAtIndex:(NSUInteger)index;
- (void)downloadManager:(DownloadManager *)downloadManager didUpdateProgressWithWrittenBytes:(unsigned long long)writtenBytes forDownloadTaskInfoAtIndex:(NSUInteger)index;

@end

NS_ASSUME_NONNULL_END
