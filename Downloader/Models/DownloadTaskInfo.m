//
//  DownloadTaskInfo.m
//  Downloader
//
//  Created by LAP12230 on 2/28/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "DownloadTaskInfo.h"

@implementation DownloadTaskInfo

- (NSArray<NSString *> *)downloadedChunkPaths {
    NSMutableArray<NSString *> *output = [[NSMutableArray alloc] init];
    for (NSURL *url in self.downloadedChunkLocations) {
        [output addObject:url.path];
    }
    
    return output;
}


- (NSString *)fileName {
    if (_fileName) {
        return _fileName;
    }
    
    // Default value
    return [self.url lastPathComponent];
}


- (instancetype)init {
    self = [super init];
    if (self) {
        
        self.numberOfChunks = 1;
        self.downloadingChunkOrder = 1;
        self.isAcceptedRanges = false;
        
        self.downloadedBytes = 0;
        self.totalBytes = 0;
        self.chunkSize = 0;
        
        self.downloadedChunkLocations = [[NSMutableArray<NSURL *> alloc] init];
        self.hasFinished = false;
    }
    
    return self;
}

@end
