//
//  DownloadCenter.h
//  Downloader
//
//  Created by LAP12230 on 2/27/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadWorker.h"
#import "DownloadTaskInfo.h"
#import "FileHelper.h"
#import "DownloadManagerDelegate.h"

typedef void (^DownloadBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface DownloadManager : NSObject

@property (nonatomic, readonly) NSMutableArray<DownloadTaskInfo *> *downloadTasks;
@property (nonatomic, weak) id <DownloadManagerDelegate> delegate;

- (void)setMaxConcurrentOperationCount:(NSInteger)count;

- (void)addDownloadTaskWith:(NSURL *)url destination:(NSURL *)destionation;
- (void)addDownloadTaskWith:(NSURL *)url destination:(NSURL *)destionation inNumberOfChunks:(NSUInteger)numberOfChunks;

@end

NS_ASSUME_NONNULL_END
