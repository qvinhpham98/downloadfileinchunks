//
//  DownloadWorker.m
//  Downloader
//
//  Created by LAP12230 on 2/27/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "DownloadWorker.h"

@interface DownloadWorker ()

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSURL *destination;
@property (nonatomic, strong) NSURLRequest *request;
@property (nonatomic, copy) CompletionHandler completionHandler;
@property (nonatomic, copy) UpdateProgressHandler updateProgressHandler;
@property (nonatomic, copy) NSString *fileName;

@end

@implementation DownloadWorker

#pragma mark - Custom Setters


- (NSString *)fileName {
    
    if (_fileName) {
        return _fileName;
    }
    
    // Default value
    NSURL *url = self.url ? self.url : self.request.URL;
    return [url.path lastPathComponent];
}


- (NSURL *)destination {
    
    if (_destination) {
        return _destination;
    }
    
    // Default value
    NSArray * urls = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    return urls[0];
}


#pragma mark - Public Methods

- (void)startDownloadTo:(NSURL *)destination fileName:(NSString *)fileName completionHandler:(CompletionHandler)completionHandler updateProgressHandler:(UpdateProgressHandler)progressHandler {
    self.fileName = fileName;
    self.destination = destination;
    [self startDownloadWithCompletionHandler:completionHandler updateProgressHandler:progressHandler];
}


- (void)startDownloadTo:(NSURL *)destination completionHandler:(CompletionHandler)completionHandler updateProgressHandler:(UpdateProgressHandler)progressHandler {
    self.destination = destination;
    [self startDownloadWithCompletionHandler:completionHandler updateProgressHandler:progressHandler];
}


- (void)startDownloadWithCompletionHandler:(CompletionHandler)completionHandler updateProgressHandler:(UpdateProgressHandler)progressHandler {
    
    self.completionHandler = completionHandler;
    self.updateProgressHandler = progressHandler;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDownloadTask *downloadTask;
    if (self.url)   downloadTask = [session downloadTaskWithURL:self.url];
    else            downloadTask = [session downloadTaskWithRequest:self.request];
    
    [downloadTask resume];
}

#pragma mark - Private Methods


/// Handle collision file name on destination url.
- (NSURL *) validDestinationURL {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    int collisionTime = 0;
    NSString *filePath = [self.destination.path stringByAppendingPathComponent:self.fileName];
    NSString *uniquePath = filePath;
    while ([fileManager fileExistsAtPath:uniquePath]) {
        collisionTime += 1;
        uniquePath = [filePath stringByAppendingFormat:@"(%d)", collisionTime];
    }
    
    return [NSURL URLWithString:uniquePath];
}


#pragma mark - Class Methods

+ (instancetype)downloadWithURL:(NSURL *)url {
    DownloadWorker *worker = [[DownloadWorker alloc] init];
    worker.url = url;
    return worker;
}


+ (instancetype)downloadWithURL:(NSURL *)url destination:(nonnull NSURL *)destination {
    DownloadWorker *worker = [DownloadWorker downloadWithURL:url];
    worker.destination = destination;
    return worker;
}


+ (instancetype)downloadWithURL:(NSURL *)url destination:(NSURL *)destination fileName:(NSString *)fileName {
    DownloadWorker *worker = [DownloadWorker downloadWithURL:url destination:destination];
    worker.fileName = fileName;
    return worker;
}


+ (instancetype)downloadWithRequest:(NSURLRequest *)request {
    DownloadWorker *worker = [[DownloadWorker alloc] init];
    worker.request = request;
    return worker;
}


+ (instancetype)downloadWithRequest:(NSURLRequest *)request destination:(NSURL *)destination {
    DownloadWorker *worker = [DownloadWorker downloadWithRequest:request];
    worker.destination = destination;
    return worker;
}


+ (instancetype)downloadWithRequest:(NSURLRequest *)request destination:(NSURL *)destination fileName:(NSString *)fileName {
    DownloadWorker *worker = [DownloadWorker downloadWithRequest:request destination:destination];
    worker.fileName = fileName;
    return worker;
}

#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(nonnull NSURLSession *)session downloadTask:(nonnull NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(nonnull NSURL *)location {
    
    NSURL *destinationURL = [self validDestinationURL];
        
    // Save downloaded file to dir.
    [[NSFileManager defaultManager] moveItemAtPath:location.path toPath:destinationURL.path error:nil];
    if (self.completionHandler) self.completionHandler(destinationURL);
    
    // Invalidate session after finishing download or it will leak memory.
    [session invalidateAndCancel];
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    if (self.updateProgressHandler) self.updateProgressHandler(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
}

@end
