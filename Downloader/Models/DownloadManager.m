//
//  DownloadCenter.m
//  Downloader
//
//  Created by LAP12230 on 2/27/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//


// TODO:
// - delegate
//

#import "DownloadManager.h"
#import "AsyncOperation.h"

static NSString * const kHeaderFieldContentLength = @"Content-Length";
static NSString * const kHeaderFieldAcceptRanges = @"Accept-Ranges";
static NSString * const kHeaderFieldRange = @"Range";

@interface DownloadManager ()

@property (nonatomic, strong) NSMutableArray<DownloadTaskInfo *> *downloadTasks;
@property (nonatomic, strong) NSOperationQueue *downloadOperationQueue;

@end

@implementation DownloadManager


#pragma mark - Life Cycles

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.downloadTasks = [[NSMutableArray<DownloadTaskInfo *> alloc] init];
        self.downloadOperationQueue = [[NSOperationQueue alloc] init];
        self.downloadOperationQueue.maxConcurrentOperationCount = 2;
    }
    return self;
}


#pragma mark - Public Methods

- (void)setMaxConcurrentOperationCount:(NSInteger)count {
    self.downloadOperationQueue.maxConcurrentOperationCount = count;
}


- (void)addDownloadTaskWith:(NSURL *)url destination:(NSURL *)destionation {
    [self addDownloadTaskWith:url destination:destionation inNumberOfChunks:1];
}


- (void)addDownloadTaskWith:(NSURL *)url destination:(NSURL *)destionation inNumberOfChunks:(NSUInteger)numberOfChunks {
    
    [self fetchDownloadInfoFrom:url completionHandler:^(DownloadTaskInfo * taskInfo) {
        
        // if download link is available
        if (taskInfo) {
            taskInfo.url = url;
            taskInfo.numberOfChunks = numberOfChunks;
            taskInfo.chunkSize = taskInfo.totalBytes / numberOfChunks;
            taskInfo.destination = destionation;
            taskInfo.index = self.downloadTasks.count;
            
            [self.downloadTasks addObject:taskInfo];
            [self addDownloadOperationsForTask:taskInfo];
        }
        
        // Error handler
        else { }
        
    }];
}


#pragma mark - Private Methods


- (void)fetchDownloadInfoFrom:(NSURL *)url completionHandler:(void (^ _Nonnull)(DownloadTaskInfo *))completionHandler {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"HEAD";
    
    NSURLSession *dSession = [NSURLSession sharedSession];
    [[dSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error) completionHandler(nil);
        
        DownloadTaskInfo *info = [[DownloadTaskInfo alloc] init];
        if ([response isKindOfClass:NSHTTPURLResponse.class]) {
            
            NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            formatter.numberStyle = NSNumberFormatterDecimalStyle;
            
            info.totalBytes = [[formatter numberFromString:(NSString *)httpResponse.allHeaderFields[kHeaderFieldContentLength]] longLongValue];
            info.isAcceptedRanges = [(NSString *)httpResponse.allHeaderFields[kHeaderFieldAcceptRanges] containsString:@"bytes"];
            
            completionHandler(info);
        }
    }] resume];
}


- (NSArray<DownloadWorker *> *)createDownloadWorkerWith:(DownloadTaskInfo *)taskInfo {
    
    NSMutableArray<DownloadWorker *> *output = [[NSMutableArray<DownloadWorker *> alloc] init];
    
    // If url support download in chunks
    if (taskInfo.numberOfChunks > 1 && taskInfo.isAcceptedRanges) {
        
        for (int i = 0; i < taskInfo.numberOfChunks; i++) {
            unsigned long long beginRange = taskInfo.chunkSize * i;
            unsigned long long endRange = beginRange + taskInfo.chunkSize - 1;
            
            // Download last bytes
            if (i == taskInfo.numberOfChunks - 1) endRange += taskInfo.chunkSize;
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:taskInfo.url];
            [request setValue:[NSString stringWithFormat:@"bytes=%lld-%lld", beginRange, endRange] forHTTPHeaderField:kHeaderFieldRange];
            
            NSString *fileName = [taskInfo.fileName stringByAppendingFormat:@".part%d", i + 1];
            DownloadWorker *worker = [DownloadWorker downloadWithRequest:request destination:taskInfo.destination fileName:fileName];
            [output addObject:worker];
        }
        
    } else {
        DownloadWorker *worker = [DownloadWorker downloadWithURL:taskInfo.url destination:taskInfo.destination fileName:taskInfo.fileName];
        [output addObject:worker];
    }
    
    return output;
}


- (NSBlockOperation *)createCompletingOperationTask:(DownloadTaskInfo *)taskInfo {
    return [NSBlockOperation blockOperationWithBlock:^{
        
        NSURL *destinationURL = [NSURL URLWithString:[taskInfo.destination.path stringByAppendingPathComponent:taskInfo.fileName]];
        
        if (taskInfo.isAcceptedRanges && taskInfo.numberOfChunks > 1) {
            
            NSLog(@"Merging task info with index %d ...", (int)taskInfo.index);
            
            [[FileHelper sharedInstance] mergeFilesAtPaths:taskInfo.downloadedChunkPaths destinationPath:taskInfo.destination.path fileName:taskInfo.fileName deletingAfterMerged:true completionHandler:^(NSString * _Nonnull path) {
                if ([self.delegate respondsToSelector:@selector(downloadManager:didFinishDownloadAndSaveAtURL:forDownloadTaskInfoAtIndex:)]) {
                    [self.delegate downloadManager:self didFinishDownloadAndSaveAtURL:destinationURL forDownloadTaskInfoAtIndex:taskInfo.index];
                }
            }];
        } else {
            if ([self.delegate respondsToSelector:@selector(downloadManager:didFinishDownloadAndSaveAtURL:forDownloadTaskInfoAtIndex:)]) {
                [self.delegate downloadManager:self didFinishDownloadAndSaveAtURL:destinationURL forDownloadTaskInfoAtIndex:taskInfo.index];
            }
        }
        
    }];
}


- (void)addDownloadOperationsForTask:(DownloadTaskInfo *)taskInfo {
    
    __weak typeof (self) weakSelf = self;
    
    // Handle merging files after finishing
    NSBlockOperation *completionOperation = [weakSelf createCompletingOperationTask:taskInfo];
    
    // Downloading chunk operation
    int chunkOrder = 0;
    
    NSArray *downloadWorkers = [self createDownloadWorkerWith:taskInfo];
    for (DownloadWorker *worker in downloadWorkers) {
        
        chunkOrder += 1;
        
        AsyncOperation *operation = [AsyncOperation initWithAsyncBlock:^(SignalFinishBlock _Nonnull finished) {
            [worker startDownloadWithCompletionHandler:^(NSURL * location) {
                
                // Caching the location
                [taskInfo.downloadedChunkLocations addObject:location];
                finished();
                
            } updateProgressHandler:^(unsigned long long additionBytes,
                                      unsigned long long downloadedBytes,
                                      unsigned long long totalBytes) {
                
                taskInfo.downloadedBytes += additionBytes;
                
                NSLog(@"Download chunk no.%d for task with index: %d", chunkOrder, (int)taskInfo.index);
                
                if ([weakSelf.delegate respondsToSelector:@selector(downloadManager:didUpdateProgressWithWrittenBytes:forDownloadTaskInfoAtIndex:)]) {
                    [weakSelf.delegate downloadManager:weakSelf didUpdateProgressWithWrittenBytes:taskInfo.downloadedBytes forDownloadTaskInfoAtIndex:taskInfo.index];
                }
            }];
        }];
        
        [self.downloadOperationQueue addOperation:operation];
        [completionOperation addDependency:operation];
    }
    
    [self.downloadOperationQueue addOperation:completionOperation];
}

@end
