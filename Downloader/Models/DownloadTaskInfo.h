//
//  DownloadTaskInfo.h
//  Downloader
//
//  Created by LAP12230 on 2/28/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DownloadTaskInfo : NSObject

@property (nonatomic, assign) NSUInteger index;

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSString *fileName;

@property (nonatomic, assign) NSUInteger numberOfChunks;
@property (nonatomic, assign) NSUInteger downloadingChunkOrder;

@property (nonatomic, assign) BOOL isAcceptedRanges;
@property (atomic, assign) unsigned long long downloadedBytes;
@property (nonatomic, assign) unsigned long long totalBytes;
@property (nonatomic, assign) unsigned long long chunkSize;

@property (nonatomic, strong) NSMutableArray<NSURL *> *downloadedChunkLocations;
@property (nonatomic, strong) NSURL *destination;
@property (nonatomic, strong) NSArray<NSString *> *downloadedChunkPaths;
@property BOOL hasFinished;

@end

NS_ASSUME_NONNULL_END
