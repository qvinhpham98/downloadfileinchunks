//
//  ChecksumHelper.h
//  Downloader
//
//  Created by LAP12230 on 2/27/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

NS_ASSUME_NONNULL_BEGIN

extern NSUInteger const kChunkSize;

@interface ChecksumHelper : NSObject

@property (nonatomic, strong) NSString *pathToFile;

- (NSString *)getMD5;

- (void)getMD5With:(void (^)(NSString *))completionHandler;

@end

NS_ASSUME_NONNULL_END
