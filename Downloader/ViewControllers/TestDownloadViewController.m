//
//  TestDownloadViewController.m
//  Downloader
//
//  Created by LAP12230 on 3/1/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "TestDownloadViewController.h"
#import "DownloadManager.h"
#import "ProgressTableViewCell.h"

@interface TestDownloadViewController ()

@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) NSString *textFieldValue;
@property (nonatomic, strong) NSURL *defaultURLDestination;

@end

@implementation TestDownloadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.downloadManager = [[DownloadManager alloc] init];
    self.downloadManager.delegate = self;
    
    self.defaultURLDestination = [NSURL URLWithString:@"/Users/lap12230/Library/Developer/CoreSimulator/Devices/E21CFDE2-773B-4034-9D7F-96DB97054EEB/data"];
    // self.textFieldValue = @"https://speed.hetzner.de/100MB.bin";
    self.textFieldValue = @"https://file-examples.com/wp-content/uploads/2017/10/file_example_JPG_2500kB.jpg";
}

- (void)textFieldDidChange:(UITextField *)textField {
    self.textFieldValue = textField.text;
}

- (IBAction)didTapButton:(UIButton *)sender {
    
    NSLog(@"Start download at URL: %@", self.textFieldValue);
    
    NSURL *url = [NSURL URLWithString:self.textFieldValue];
    [self.downloadManager addDownloadTaskWith:url destination:self.defaultURLDestination inNumberOfChunks:10];
    [self.tableView reloadData];
}


#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // return self.downloadManager.downloadTasks.count;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProgressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"progressCell"];
    DownloadTaskInfo *info = self.downloadManager.downloadTasks[indexPath.item];
    cell.textLabel.text = info.url.path;
    
    float progress = info.downloadedBytes / info.totalBytes;
    cell.progressView.progress = progress;
    return cell;
}

- (void)downloadManager:(nonnull DownloadManager *)downloadManager didFinishDownloadAndSaveAtURL:(nonnull NSURL *)url forDownloadTaskInfoAtIndex:(NSUInteger)index {
    NSLog(@"Finished: %d", (int)index);
}

- (void)downloadManager:(nonnull DownloadManager *)downloadManager didUpdateProgressWithWrittenBytes:(unsigned long long)writtenBytes forDownloadTaskInfoAtIndex:(NSUInteger)index {
    // DownloadTaskInfo *info = self.downloadManager.downloadTasks[index];
    // NSLog(@"Update at index %d: %lld - %lld", (int)index, writtenBytes, info.totalBytes);
    
}

@end
