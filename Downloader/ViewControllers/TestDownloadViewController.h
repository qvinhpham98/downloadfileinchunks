//
//  TestDownloadViewController.h
//  Downloader
//
//  Created by LAP12230 on 3/1/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownloadManagerDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface TestDownloadViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, DownloadManagerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
