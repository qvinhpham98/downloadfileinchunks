//
//  FileHelper.h
//  DemoFileExplorer
//
//  Created by LAP12230 on 2/25/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString *const kFileHelperFinishedOperatingNotification;
extern NSString *const kFileHelperUpdateTruncatingProgressNotification;
extern NSString *const kFileHelperUpdateMergingProgressNotification;

@interface FileHelper : NSObject

@property (atomic, assign) BOOL operating;

+ (instancetype)sharedInstance;

- (void)mergeFilesAtPaths:(NSArray<NSString *> *)paths destinationPath:(NSString *)dPath deletingAfterMerged:(BOOL)deletingAfterMerged completionHandler:(void (^)(NSString *))completionHandler;

- (void)mergeFilesAtPaths:(NSArray<NSString *> *)paths destinationPath:(NSString *)dPath fileName:(NSString *)fileName deletingAfterMerged:(BOOL)deletingAfterMerged completionHandler:(void (^)(NSString * _Nonnull))completionHandler;

- (void)truncateFileAtPath:(NSString *)path destinationPath:(NSString *)dPath chunkSizeInMb:(float)chunkSizeInMb;

@end

NS_ASSUME_NONNULL_END


