//
//  ProgressTableViewCell.h
//  Downloader
//
//  Created by LAP12230 on 3/1/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProgressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@end
NS_ASSUME_NONNULL_END
