//
//  ChecksumHelper.m
//  Downloader
//
//  Created by LAP12230 on 2/27/20.
//  Copyright © 2020 LAP12230. All rights reserved.
//

#import "ChecksumHelper.h"

NSUInteger const kChunkSize = 8000;

@implementation ChecksumHelper

- (NSString *)getMD5 {
    
    // Validate if file exists
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:self.pathToFile];
    if (!handle) return nil;
    
    CC_MD5_CTX md5;
    CC_MD5_Init(&md5);
    
    BOOL done = false;
    while (!done) {
        @autoreleasepool {
            NSData *data = [handle readDataOfLength:kChunkSize];
            CC_MD5_Update(&md5, data.bytes, (unsigned int)data.length);
            if (data.length == 0) done = true;
        }
    }
    
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5_Final(digest, &md5);
    
    NSString *output = [NSMutableString stringWithString:@""];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        output = [output stringByAppendingFormat:@"%02x", digest[i]];
    }
    
    return output;
}


- (void)getMD5With:(void (^)(NSString * _Nonnull))completionHandler {
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0), ^{
        
        // Validate if file exists
        NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:self.pathToFile];
        if (!handle) return;
        
        CC_MD5_CTX md5;
        CC_MD5_Init(&md5);
        
        BOOL done = false;
        while (!done) {
            @autoreleasepool {
                NSData *data = [handle readDataOfLength:kChunkSize];
                CC_MD5_Update(&md5, data.bytes, (unsigned int)data.length);
                if (data.length == 0) done = true;
            }
        }
        
        unsigned char digest[CC_MD5_DIGEST_LENGTH];
        CC_MD5_Final(digest, &md5);
        NSString *output = @"";
        
        for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
            [output stringByAppendingFormat:@"%02x", digest[i]];
        }
        
        completionHandler(output);
        
    });
    
}

@end
